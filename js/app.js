(function($, document, window) {

    $(document).ready(function() {

        // Cloning main navigation for mobile menu
        $(".mobile-navigation").append($(".main-navigation .menu").clone());

        // Mobile menu toggle 
        $(".menu-toggle").click(function() {
            $(".mobile-navigation").slideToggle();
        });
    });

    $(window).load(function() {

    });

})(jQuery, document, window);

const dayName = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

const monthName = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];

const btnFind = document.getElementById('btnFind');
const cityElement = document.getElementById('city');
const dayElement = document.getElementsByClassName('day');
const tempElements = document.getElementsByClassName('temp');
const icons = document.getElementsByClassName('weather-icon');

const defaultCity = 'Mumbai';
let cityName ;

btnFind.addEventListener('click', function (evt) {
    evt.preventDefault();
    getCityName();
    getAndDisplayWeatherDetails(cityName);
} );

const getCityName = () => {
    cityName = cityElement.value.length === 0 ? defaultCity : cityElement.value;
    console.log(cityName); 
}

function getAndDisplayWeatherDetails(cityName)
{
    getForeCast(cityName)
        .then(data => processReceivedData(data))
        .catch(err => alert("Something went wrong: "+err));
}

function processReceivedData(data) {
    data = data.list;
    document.getElementById("location").innerHTML = cityName;
    const imagePath = "images/icons";
    for(let i = 0, j = 0; i < data.length; i+=8, j++) {
        tempElements[j].innerHTML = Math.round(data[i].main.temp) + "<sup>o</sup>C";
        const dateObj = new Date(data[i].dt_txt);
        const day = dayName[dateObj.getDay()];
        const month = monthName[dateObj.getMonth()];
        dayElement[j].innerHTML = day;
        
        if(j === 0){
            document.getElementById("date").innerHTML = dateObj.getDate() + '' + month;
            document.getElementById("humidity").innerHTML = Math.round(data[i].main.humidity) + "%";
            document.getElementById("wind-speed").innerHTML = Math.round(data[i].wind.speed *3.6) +"km/h" ;
            document.getElementById("wind-degree").innerHTML = (data[i].wind.deg + "<sup>o</sup>")
        }

        icons[j].src = `${imagePath}/${data[i].weather[0].icon}.svg`;
    }
}