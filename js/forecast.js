const API_KEY = 'c5855a88e899d895b5eb6fafdfc134d7';
const BASE_URL = 'https://api.openweathermap.org/data/2.5/forecast';
const units = {
    fahren: 'imperial',
    celsius: 'metric',
    default: 'standard'
};

const getForeCast = async(cityName) => {
    const URL = BASE_URL + `?q=${cityName}&appid=${API_KEY}&units=${units.celsius}`;
    const res = await fetch(URL);
    if(res.ok){
        const data = await res.json();
        return data;
    }
    throw new Error(res.status);
}

// getForeCast('Mumbai').then((data) => {
//     console.log(data);
// });